/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.datamodel;

import java.util.List;

public class DeltaVectors {
	private List<String> periods;
	private List<CurveDelta> curves;

	public DeltaVectors(List<String> periods, List<CurveDelta> curves) {
		this.periods = periods;
		this.curves = curves;
	}
	
	public List<String> getPeriods() {
		return periods;
	}
	public void setPeriods(List<String> periods) {
		this.periods = periods;
	}
	public List<CurveDelta> getCurves() {
		return curves;
	}
	public void setCurves(List<CurveDelta> curves) {
		this.curves = curves;
	}
}
