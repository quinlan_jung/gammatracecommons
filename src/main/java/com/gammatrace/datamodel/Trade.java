/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.datamodel;

import org.threeten.bp.Period;

import com.gammatrace.commonutils.TimeUtils;
import com.gammatrace.commonutils.TradeUtils;

public class Trade {
    private long dissemination_id;
    private Long original_dissemination_id;
    private String action;
    private Long execution_timestamp;
    private String cleared;
    private String indication_of_collateralization;
    private String indication_of_end_user_exception;
    private String indication_of_other_price_affecting_term;
    private String block_trades_and_large_notional_off_facility_swaps;
    private String execution_venue;
    private Long effective_date;
    private Long end_date;
    private String day_count_convention;
    private String settlement_currency;
    private String asset_class;
    private String sub_asset_class_for_other_commodity;
    private String taxonomy;
    private String price_forming_continuation_data;
    private String underlying_asset_1;
    private String underlying_asset_2;
    private String price_notation_type;
    private Double price_notation;
    private String additional_price_notation_type;
    private Double additional_price_notation;
    private String notional_currency_1;
    private String notional_currency_2;
    private Long rounded_notional_amount_1; // we split up dtcc's roundednotionalamount to a long (notionalamount_1) and an overflow flag (t/f)
    private Boolean rounded_notional_overflow_flag_1; // only found in our database
    private Long rounded_notional_amount_2;
    private Boolean rounded_notional_overflow_flag_2;// only found in our database
    private String payment_frequency_1;
    private String payment_frequency_2;
    private String reset_frequency_1;
    private String reset_frequency_2;
    private String embeded_option;
    private Double option_strike_price;
    private String option_type;
    private String option_family;
    private String option_currency;
    private Double option_premium;
    private String option_lock_period;
    private Long option_expiration_date;
    private String price_notation2_type;
    private Double price_notation2;
    private String price_notation3_type;
    private Double price_notation3;
    
    public Trade () {}
    
    private Period convertPeriod(String date){
    	return date.isEmpty() ? null : TimeUtils.parseDate(date);
    }
    
    private double convertNotationRate(Double notation, String notationType){
    	return notation == null ? 0.0d : TradeUtils.rateConverter(notation, notationType);
    }
    
	public long getDissemination_id() {
		return dissemination_id;
	}
	public void setDissemination_id(long dissemination_id) {
		this.dissemination_id = dissemination_id;
	}
	public Long getOriginal_dissemination_id() {
		return original_dissemination_id;
	}
	public void setOriginal_dissemination_id(Long original_dissemination_id) {
		this.original_dissemination_id = original_dissemination_id;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Long getExecution_timestamp() {
		return execution_timestamp;
	}
	public void setExecution_timestamp(Long execution_timestamp) {
		this.execution_timestamp = execution_timestamp;
	}
	public String getCleared() {
		return cleared;
	}
	public void setCleared(String cleared) {
		this.cleared = cleared;
	}
	public String getIndication_of_collateralization() {
		return indication_of_collateralization;
	}
	public void setIndication_of_collateralization(String indication_of_collateralization) {
		this.indication_of_collateralization = indication_of_collateralization;
	}
	public String getIndication_of_end_user_exception() {
		return indication_of_end_user_exception;
	}
	public void setIndication_of_end_user_exception(String indication_of_end_user_exception) {
		this.indication_of_end_user_exception = indication_of_end_user_exception;
	}
	public String getIndication_of_other_price_affecting_term() {
		return indication_of_other_price_affecting_term;
	}
	public void setIndication_of_other_price_affecting_term(String indication_of_other_price_affecting_term) {
		this.indication_of_other_price_affecting_term = indication_of_other_price_affecting_term;
	}
	public String getBlock_trades_and_large_notional_off_facility_swaps() {
		return block_trades_and_large_notional_off_facility_swaps;
	}
	public void setBlock_trades_and_large_notional_off_facility_swaps(
			String block_trades_and_large_notional_off_facility_swaps) {
		this.block_trades_and_large_notional_off_facility_swaps = block_trades_and_large_notional_off_facility_swaps;
	}
	public String getExecution_venue() {
		return execution_venue;
	}
	public void setExecution_venue(String execution_venue) {
		this.execution_venue = execution_venue;
	}
	public Long getEffective_date() {
		return effective_date;
	}
	public void setEffective_date(Long effective_date) {
		this.effective_date = effective_date;
	}
	public Long getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Long end_date) {
		this.end_date = end_date;
	}
	public String getDay_count_convention() {
		return day_count_convention;
	}
	public void setDay_count_convention(String day_count_convention) {
		this.day_count_convention = day_count_convention;
	}
	public String getSettlement_currency() {
		return settlement_currency;
	}
	public void setSettlement_currency(String settlement_currency) {
		this.settlement_currency = settlement_currency;
	}
	public String getAsset_class() {
		return asset_class;
	}
	public void setAsset_class(String asset_class) {
		this.asset_class = asset_class;
	}
	public String getSub_asset_class_for_other_commodity() {
		return sub_asset_class_for_other_commodity;
	}
	public void setSub_asset_class_for_other_commodity(String sub_asset_class_for_other_commodity) {
		this.sub_asset_class_for_other_commodity = sub_asset_class_for_other_commodity;
	}
	public String getTaxonomy() {
		return taxonomy;
	}
	public void setTaxonomy(String taxonomy) {
		this.taxonomy = taxonomy;
	}
	public String getPrice_forming_continuation_data() {
		return price_forming_continuation_data;
	}
	public void setPrice_forming_continuation_data(String price_forming_continuation_data) {
		this.price_forming_continuation_data = price_forming_continuation_data;
	}
	public String getUnderlying_asset_1() {
		return underlying_asset_1;
	}
	public void setUnderlying_asset_1(String underlying_asset_1) {
		this.underlying_asset_1 = underlying_asset_1;
	}
	public String getUnderlying_asset_2() {
		return underlying_asset_2;
	}
	public void setUnderlying_asset_2(String underlying_asset_2) {
		this.underlying_asset_2 = underlying_asset_2;
	}
	public String getPrice_notation_type() {
		return price_notation_type;
	}
	public void setPrice_notation_type(String price_notation_type) {
		this.price_notation_type = price_notation_type;
	}
	public Double getPrice_notation() {
		return convertNotationRate(price_notation, price_notation_type);
	}
	public void setPrice_notation(Double price_notation) {
		this.price_notation = price_notation;
	}
	public String getAdditional_price_notation_type() {
		return additional_price_notation_type;
	}
	public void setAdditional_price_notation_type(String additional_price_notation_type) {
		this.additional_price_notation_type = additional_price_notation_type;
	}
	public Double getAdditional_price_notation() {
		return additional_price_notation;
	}
	public void setAdditional_price_notation(Double additional_price_notation) {
		this.additional_price_notation = additional_price_notation;
	}
	public String getNotional_currency_1() {
		return notional_currency_1;
	}
	public void setNotional_currency_1(String notional_currency_1) {
		this.notional_currency_1 = notional_currency_1;
	}
	public String getNotional_currency_2() {
		return notional_currency_2;
	}
	public void setNotional_currency_2(String notional_currency_2) {
		this.notional_currency_2 = notional_currency_2;
	}
	public Long getRounded_notional_amount_1() {
		return rounded_notional_amount_1;
	}
	public void setRounded_notional_amount_1(Long rounded_notional_amount_1) {
		this.rounded_notional_amount_1 = rounded_notional_amount_1;
	}
	public Boolean getRounded_notional_overflow_flag_1() {
		return rounded_notional_overflow_flag_1;
	}
	public void setRounded_notional_overflow_flag_1(Boolean rounded_notional_overflow_flag_1) {
		this.rounded_notional_overflow_flag_1 = rounded_notional_overflow_flag_1;
	}
	public Long getRounded_notional_amount_2() {
		return rounded_notional_amount_2;
	}
	public void setRounded_notional_amount_2(Long rounded_notional_amount_2) {
		this.rounded_notional_amount_2 = rounded_notional_amount_2;
	}
	public Boolean getRounded_notional_overflow_flag_2() {
		return rounded_notional_overflow_flag_2;
	}
	public void setRounded_notional_overflow_flag_2(Boolean rounded_notional_overflow_flag_2) {
		this.rounded_notional_overflow_flag_2 = rounded_notional_overflow_flag_2;
	}
	public Period getPayment_frequency_1_asPeriod() {
		return convertPeriod(payment_frequency_1);
	}
	public String getPayment_frequency_1() {
		return payment_frequency_1;
	}
	public void setPayment_frequency_1(String payment_frequency_1) {
		this.payment_frequency_1 = payment_frequency_1;
	}
	public Period getPayment_frequency_2_asPeriod() {
		return convertPeriod(payment_frequency_2);
	}
	public String getPayment_frequency_2() {
		return payment_frequency_2;
	}
	public void setPayment_frequency_2(String payment_frequency_2) {
		this.payment_frequency_2 = payment_frequency_2;
	}
	public Period getReset_frequency_1_asPeriod() {
		return convertPeriod(reset_frequency_1);
	}
	public String getReset_frequency_1() {
		return reset_frequency_1;
	}
	public void setReset_frequency_1(String reset_frequency_1) {
		this.reset_frequency_1 = reset_frequency_1;
	}
	public Period getReset_frequency_2_asPeriod() {
		return convertPeriod(reset_frequency_2);
	}
	public String getReset_frequency_2() {
		return reset_frequency_2;
	}
	public void setReset_frequency_2(String reset_frequency_2) {
		this.reset_frequency_2 = reset_frequency_2;
	}
	public String getEmbeded_option() {
		return embeded_option;
	}
	public void setEmbeded_option(String embeded_option) {
		this.embeded_option = embeded_option;
	}
	public Double getOption_strike_price() {
		return option_strike_price;
	}
	public void setOption_strike_price(Double option_strike_price) {
		this.option_strike_price = option_strike_price;
	}
	public String getOption_type() {
		return option_type;
	}
	public void setOption_type(String option_type) {
		this.option_type = option_type;
	}
	public String getOption_family() {
		return option_family;
	}
	public void setOption_family(String option_family) {
		this.option_family = option_family;
	}
	public String getOption_currency() {
		return option_currency;
	}
	public void setOption_currency(String option_currency) {
		this.option_currency = option_currency;
	}
	public Double getOption_premium() {
		return option_premium;
	}
	public void setOption_premium(Double option_premium) {
		this.option_premium = option_premium;
	}
	public String getOption_lock_period() {
		return option_lock_period;
	}
	public void setOption_lock_period(String option_lock_period) {
		this.option_lock_period = option_lock_period;
	}
	public Long getOption_expiration_date() {
		return option_expiration_date;
	}
	public void setOption_expiration_date(Long option_expiration_date) {
		this.option_expiration_date = option_expiration_date;
	}
	public String getPrice_notation2_type() {
		return price_notation2_type;
	}
	public void setPrice_notation2_type(String price_notation2_type) {
		this.price_notation2_type = price_notation2_type;
	}
	public double getPrice_notation2() {
		return convertNotationRate(price_notation2, price_notation2_type);
	}
	public void setPrice_notation2(Double price_notation2) {
		this.price_notation2 = price_notation2;
	}
	public String getPrice_notation3_type() {
		return price_notation3_type;
	}
	public void setPrice_notation3_type(String price_notation3_type) {
		this.price_notation3_type = price_notation3_type;
	}
	public Double getPrice_notation3() {
		return price_notation3;
	}
	public void setPrice_notation3(Double price_notation3) { // TODO: convert notation rate as well?
		this.price_notation3 = price_notation3;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (dissemination_id ^ (dissemination_id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trade other = (Trade) obj;
		if (dissemination_id != other.dissemination_id)
			return false;
		return true;
	}
    
    
}
