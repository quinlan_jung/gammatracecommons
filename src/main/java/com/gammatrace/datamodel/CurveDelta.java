/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.datamodel;

import java.util.List;

public class CurveDelta {
	String name;
	List<Double> values;

	public CurveDelta(String name, List<Double> values) {
		this.name = name;
		this.values = values;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Double> getValues() {
		return values;
	}
	public void setValues(List<Double> values) {
		this.values = values;
	}

}
