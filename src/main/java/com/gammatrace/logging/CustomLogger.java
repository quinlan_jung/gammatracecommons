/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.logging;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;


public class CustomLogger {
	static boolean init = false;
	static final Path loggingPath = Paths.get("./logs");
	
	public static Path getLoggingPath(){
		return loggingPath;
	}
	
	public static void init(){
		if (init){
			return;
		}
		
		// TODO: do something about this try catch
		try {
			Files.createDirectories(loggingPath);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		// creates pattern layout
        PatternLayout layout = new PatternLayout();
        String conversionPattern = "[%p] %d %c %M - %m%n";
        layout.setConversionPattern(conversionPattern);
 
        // creates daily rolling file appender
        DailyRollingFileAppender rollingAppender = new DailyRollingFileAppender();
        rollingAppender.setFile("./logs/app.log");
        rollingAppender.setDatePattern("'.'yyyy-MM-dd");
        rollingAppender.setLayout(layout);
        rollingAppender.activateOptions();
 
        //setup console appender
        ConsoleAppender consoleAppender = new ConsoleAppender(); //create appender
        consoleAppender.setLayout(layout);
        consoleAppender.activateOptions();
        
        // configures the root logger
        Logger rootLogger = Logger.getRootLogger();
        rootLogger.setLevel(Level.INFO);
        rootLogger.addAppender(rollingAppender);
        rootLogger.addAppender(consoleAppender);
        
        init = true;
	}

	
	public static Logger getLogger(Class<?> clazz) {
		init();
		return Logger.getLogger(clazz);
	}
}
