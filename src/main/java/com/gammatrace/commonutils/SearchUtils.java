/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.commonutils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SearchUtils {
		// returns positive number if lowest negative difference is found
		// returns 0 or negative if it isnt found
		static public int findLowestDifferenceIndex(int [] sortedList, int timestamp){
			int searchResult = Arrays.binarySearch(sortedList, timestamp);
			if (searchResult >= 0){
				return searchResult;
			}
			else{
				return ((searchResult + 1) * -1) - 1;
			}
		}
		
		static public <K> int findLowestDifferenceIndex(List<K> sortedList, K key){
			int searchResult = Collections.binarySearch(sortedList, key, null);
			if (searchResult >= 0){
				return searchResult;
			}
			else{
				return ((searchResult + 1) * -1) - 1;
			}
		}
		
		// returns the position of timestamp in array
		// if timestamp doesnt exist, return the position it would've been inserted at
		static public int binarySearch(int [] sortedList, int timestamp){
			int searchResult = Arrays.binarySearch(sortedList, timestamp);
			if (searchResult >= 0){
				return searchResult;
			}
			else{
				return (searchResult + 1) * -1;
			}
		}
}
