/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.commonutils;

import java.util.regex.Pattern;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.threeten.bp.Period;

public class TimeUtils {
	// epoch seconds for time now in UTC
	static public long now(){
		return new DateTime(DateTimeZone.UTC).getMillis() / DateTimeConstants.MILLIS_PER_SECOND;
	}
	
	static public long toSeconds(DateTime dt){
		return dt.getMillis() / DateTimeConstants.MILLIS_PER_SECOND;
	}
	
	static public DateTime toDateTime(long epochSeconds){
		return new DateTime(epochSeconds * DateTimeConstants.MILLIS_PER_SECOND, DateTimeZone.UTC);
	}
	
	public static Period[] parseDate(String[] date) throws IllegalArgumentException {
		Period[] result = new Period[date.length];
		for (int i = 0; i < date.length; i++) {
			result[i] = parseDate(date[i]);
		}
		return result;
	}
	
	// Return a period that corresponds to the String representation
	public static Period parseDate(String date) throws IllegalArgumentException {
		// We will see stuff like 1D, 1M, 1Y
		Pattern period = Pattern.compile("(\\d++)([D|M|Y|T])");  // review whether we can handle 1T like this
		java.util.regex.Matcher matcher = period.matcher(date);
		while (matcher.find()) {
			int number = Integer.parseInt(matcher.group(1));
			String letter = matcher.group(2);
			return getPeriod(number, letter);
		}
		throw new IllegalArgumentException("Invalid period given: " + date);
	}

	public static Period getPeriod(int number, String letter) {
		if (letter.equals("D")) {
			return Period.ofDays(number);
		} else if (letter.equals("M")) {
			return Period.ofMonths(number);
		} else if (letter.equals("Y")) {
			return Period.ofYears(number);
		} else if (letter.equals("T")){
			return Period.ofYears(number);
		}
		return null;
	}
}
