/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.commonutils;

import java.util.HashSet;
import java.util.Set;

import com.gammatrace.datamodel.Trade;

public class SupportedUtils {
	static Set<String> supportedCurrencies = new HashSet<String>();
	static {
		supportedCurrencies.add("EUR");
		supportedCurrencies.add("GBP");
		supportedCurrencies.add("JPY");
		supportedCurrencies.add("USD");
	}
	
	public static boolean isSupportedCurrency(String currency){
		return supportedCurrencies.contains(currency);
	}
	
	public static boolean isSupportedCurrency(Trade trade){
		boolean notionalCurrency1Supported = supportedCurrencies.contains(trade.getNotional_currency_1()) || trade.getNotional_currency_1().isEmpty();
		boolean notionalCurrency2Supported = supportedCurrencies.contains(trade.getNotional_currency_2()) || trade.getNotional_currency_2().isEmpty();
		return notionalCurrency1Supported && notionalCurrency2Supported;
	}
}
