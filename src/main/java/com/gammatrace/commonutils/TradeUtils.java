/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.commonutils;

import org.threeten.bp.Period;

import com.gammatrace.datamodel.Trade;

public class TradeUtils {

    private static boolean isFixed(String underlyingAsset){
    	return underlyingAsset.equalsIgnoreCase("FIXED");
    }
    
	// TODO: create a convertion class to contain all of these things..
	public static double rateConverter (Double rate, String notation) {
		if (notation.equalsIgnoreCase("Percent")) {
			return rate/100;
		} else if (notation.equalsIgnoreCase("Spread")) {
			return rate/100;
		} else if (notation.equalsIgnoreCase("BasisPoints")) {
			return rate/10000;
		} else {
			return rate; // TODO: this is definitely bad, find something better
		}
	}
    
    /**
     * @assume: the trade is of a type that has a fixed/float leg
     * Returns the fixed payment period, throwing IllegalArgumentException if there is no fixed leg
     */
	public static Period getFixedPaymentPeriod(Trade trade) {	
		if (isFixed(trade.getUnderlying_asset_1())) {
			return trade.getPayment_frequency_1_asPeriod();
		} else if (isFixed(trade.getUnderlying_asset_2())){
			return trade.getPayment_frequency_2_asPeriod();
		} else {
			throw new IllegalArgumentException("One leg must have asset name of fixed in a FixedIbor swap");
		}
	}
	
    /**
     * @assume: the trade is of a type that has a fixed/float leg
     * Returns the float payment period, throwing IllegalArgumentException if there is no fixed leg
     */
	public static Period getFloatPaymentPeriod(Trade trade) {	
		if (isFixed(trade.getUnderlying_asset_1())) {
			return trade.getPayment_frequency_2_asPeriod();
		} else if (isFixed(trade.getUnderlying_asset_2())){
			return trade.getPayment_frequency_1_asPeriod();
		} else {
			throw new IllegalArgumentException("One leg must have asset name of fixed in a FixedIbor swap");
		}
	}
	
    /**
     * @assume: the trade is of a type that has a fixed/float leg
     * Returns the float reset period, null if not period is empty, and throwing IllegalArgumentException if there is no fixed leg
     */
	public static Period getFloatResetPeriod(Trade trade) {	
		if (isFixed(trade.getUnderlying_asset_1())) {
			return trade.getReset_frequency_2_asPeriod();
		} else if (isFixed(trade.getUnderlying_asset_2())){
			return trade.getReset_frequency_1_asPeriod();
		} else {
			throw new IllegalArgumentException("One leg must have asset name of fixed in a FixedIbor swap");
		}
	}
	
	/**
     * @assume: the trade is of a type that has a fixed/float leg
     * Returns the fixed leg notional amount, null if not period is empty, and throwing IllegalArgumentException if there is no fixed leg
     */
	public static double getFixedNotional(Trade trade) {
		if (isFixed(trade.getUnderlying_asset_1())) {
			return trade.getRounded_notional_amount_1();
		} else if (isFixed(trade.getUnderlying_asset_2())){
			return trade.getRounded_notional_amount_2();
		} else {
			throw new IllegalArgumentException("One leg must have asset name of fixed in a FixedIbor swap");
		}
	}
	
	/**
     * @assume: the trade is of a type that has a fixed/float leg
     * Returns the fixed leg notional amount, null if not period is empty, and throwing IllegalArgumentException if there is no fixed leg
     */
	public static double getFloatNotional(Trade trade) {
		if (isFixed(trade.getUnderlying_asset_1())) {
			return trade.getRounded_notional_amount_2();
		} else if (isFixed(trade.getUnderlying_asset_2())){
			return trade.getRounded_notional_amount_1();
		} else {
			throw new IllegalArgumentException("One leg must have asset name of fixed in a FixedIbor swap");
		}
	}
	
}
