/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.commonutils;

import java.util.List;

public class StringUtils {
	public static <T> String generateDelimitedString(List<T> collection, String delimiter, String prefix) {
		StringBuilder sb = new StringBuilder();	
		for (int i = 0; i < collection.size(); i++){
			if (i > 0){
				sb.append(delimiter);
				sb.append(" ");
			}
			if (prefix != null && !prefix.equals("")){
				sb.append(prefix);
			}
			sb.append(collection.get(i));
		}
		return sb.toString();
	}
	
	public static <T> String generateDelimitedString(Iterable<T> collection, String delimiter, String prefix){
		StringBuilder sb = new StringBuilder();
		boolean sawFirst = false;
		for (T i : collection) {
			if (sawFirst) {
				sb.append(delimiter);
				sb.append(" ");
			}
			if (prefix != null && !prefix.equals("")){
				sb.append(prefix);
			}
			sb.append(i);
			sawFirst = true;
		}

		return sb.toString();
	}
	
	public static <T> String generateDelimitedString(T [] array, String delimiter, String prefix){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++){
			if (i > 0){
				sb.append(delimiter);
				sb.append(" ");
			}
			if (prefix != null && !prefix.equals("")){
				sb.append(prefix);
			}
			sb.append(array[i]);
		}
		return sb.toString();
	}
	
	public static String [] getColumnsAsString(Object [] objectArray){
		String [] stringArray = new String [objectArray.length];
		for (int i = 0; i < objectArray.length; i++){
			stringArray[i] = objectArray[i].toString();
		}
		return stringArray;
	}
}
