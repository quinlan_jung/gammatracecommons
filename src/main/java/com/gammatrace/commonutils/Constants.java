/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.commonutils;
import java.util.regex.Pattern;


public class Constants {
	public static final String EMPTY_STRING = "";
	public static final Pattern NOTIONAL = Pattern.compile("(\\d++)(\\+??)");
}
