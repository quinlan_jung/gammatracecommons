/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.commonutils;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.threeten.bp.Period;

import com.gammatrace.logging.CustomLogger;

public class Utils {
	static Logger logger = CustomLogger.getLogger(Utils.class);
	
	// Return an array C concatenating A and B, in that order
	public static <T> T[] concatenate (T[] A, T[] B) {
	    int aLen = A.length;
	    int bLen = B.length;

	    @SuppressWarnings("unchecked")
	    T[] C = (T[]) Array.newInstance(A.getClass().getComponentType(), aLen+bLen);
	    System.arraycopy(A, 0, C, 0, aLen);
	    System.arraycopy(B, 0, C, aLen, bLen);

	    return C;
	}
	
	public static <K, V> void putInMapList(Map<K, List<V>> map, K key, V value){
		List<V> list;
		if (map.containsKey(key)){
			list = map.get(key);
			list.add(value);
		}
		else{
			list = new ArrayList<V> ();
			list.add(value);
			map.put(key, list);
		}
	}
	
	public static void clearDirectory(Path path){
		if (!Files.isDirectory(path)){
			return;
		}
		try (DirectoryStream<Path> ds = Files.newDirectoryStream(path)) {
			for (Path p : ds) {
				Files.deleteIfExists(p);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @return true if a is approximately equal to b (within the epsilon)
	 */
	public static boolean fuzzyEquals(Double a, Double b, Double epsilon){
		return Math.abs(a-b) < epsilon;
	}
	
	/**
	 * @return true if a is approximately equal to b (within 0.0000001)
	 */
	public static boolean fuzzyEquals(Double a, Double b){
		return Math.abs(a-b) < 0.0000001;
	}
	
	// Sleep for the specified value, in units
	public static void delay(int value, TimeUnit units){
		int millis = (int) TimeUnit.MILLISECONDS.convert(value, units);
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			logger.error(String.format("Thread was interrupted, tried to sleep for %d millis", millis));
		}
	}
	
    // round down rawNumber to the nearest multiple
	static public long roundDown (long rawNumber, long nearestMultiple){
		return rawNumber - (rawNumber % nearestMultiple);
	}
}
