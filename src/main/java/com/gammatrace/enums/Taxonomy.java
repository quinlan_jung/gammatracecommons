/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.enums;

import java.util.HashSet;
import java.util.Set;

/**
 * The Dtcc Taxonomies that we are repricing
 *
 */
public enum Taxonomy {
 INTERESTRATE_IRSWAP_FIXEDFLOAT("InterestRate:IRSwap:FixedFloat"),
 INTERESTRATE_IRSWAP_OIS("InterestRate:IRSwap:OIS"),
 INTERESTRATE_IRSWAP_BASIS("InterestRate:IRSwap:Basis"),
 INTERESTRATE_CROSSCURRENCY_BASIS("InterestRate:CrossCurrency:Basis"),
 INTERESTRATE_CROSSCURRENCY_FIXEDFLOAT("InterestRate:CrossCurrency:FixedFloat"),
 INTERESTRATE_CROSSCURRENCY_FIXEDFIXED("InterestRate:CrossCurrency:FixedFixed"),
 INTERESTRATE_OPTION_SWAPTION("InterestRate:Option:Swaption"),
 INTERESTRATE_CAPFLOOR("InterestRate:CapFloor");
 
 private String taxonomy;
 private Taxonomy(String taxonomy){
	 this.taxonomy = taxonomy;
 }
 
 /**
  *@param taxonomy: the Dtcc formatted taxonomy String (ie) InterestRate:IRSwap:FixedFloat
  *@return the mapped Taxonomy
  *
  */
 public static Taxonomy toEnum(String taxonomy){
	 return Taxonomy.valueOf(taxonomy.toUpperCase().replace(":", "_"));
 }
 
 /**
  *@return String values for all Taxonomies
  */
 public static Set<String> stringValues(){
	 Taxonomy [] taxonomies = Taxonomy.values();
	 Set<String> taxonomyStrings = new HashSet<String> ();
	 for (Taxonomy t : taxonomies){
		 taxonomyStrings.add(t.toString());
	 }
	 return taxonomyStrings;
 }
 
 @Override
 /**
  *@return Dtcc String format of Taxonomy
  */
 public String toString(){
	 return taxonomy;
 }
}
