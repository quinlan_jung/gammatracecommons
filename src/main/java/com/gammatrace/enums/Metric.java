/*******************************************************************************
 * Copyright (c) 2014, 2015 Quinlan Jung.
 * All rights reserved.
 *******************************************************************************/
package com.gammatrace.enums;


public enum Metric {
	COMMON_FIXED_FAIR_RATE(Double.class),
	END_DATE_FIXED_FAIR_RATE(Double.class),
	EFFECTIVE_DATE_FIXED_FAIR_RATE(Double.class),
	SPREAD_DELTA(String.class),
	FIXED_DELTA(String.class),
	XCCY_DELTA(String.class),
	VEGA_MATRIX(String.class),
	VOLATILITY(Double.class),
	FLAT_DELTA(String.class);
	
	private final Class<?> clazz;
	Metric(Class<?> clazz) {
        this.clazz = clazz;
    }
	
	public Class<?> getType(){
		return clazz;
	}
	
	/**
	 * All the columns from DTCC, in order.
	 */
	public static final Metric[] METRIC_ARRAY = { COMMON_FIXED_FAIR_RATE, END_DATE_FIXED_FAIR_RATE, EFFECTIVE_DATE_FIXED_FAIR_RATE, SPREAD_DELTA, FIXED_DELTA, XCCY_DELTA, VEGA_MATRIX, VOLATILITY, FLAT_DELTA};
	
	public static final Metric [] METRIC_INTERESTRATE_IRSWAP_FIXEDFLOAT_COLUMN_ARRAY = {COMMON_FIXED_FAIR_RATE, END_DATE_FIXED_FAIR_RATE, EFFECTIVE_DATE_FIXED_FAIR_RATE, SPREAD_DELTA, FIXED_DELTA, FLAT_DELTA};
	public static final Metric [] METRIC_INTERESTRATE_IRSWAP_OIS_COLUMN_ARRAY = {COMMON_FIXED_FAIR_RATE, END_DATE_FIXED_FAIR_RATE, EFFECTIVE_DATE_FIXED_FAIR_RATE, SPREAD_DELTA, FIXED_DELTA, FLAT_DELTA};
	public static final Metric [] METRIC_INTERESTRATE_XCCY_COLUMN_ARRAY = {COMMON_FIXED_FAIR_RATE, END_DATE_FIXED_FAIR_RATE, EFFECTIVE_DATE_FIXED_FAIR_RATE, SPREAD_DELTA, FIXED_DELTA, XCCY_DELTA, FLAT_DELTA};
	public static final Metric [] METRIC_INTERESTRATE_OPTION_COLUMN_ARRAY = {SPREAD_DELTA, FIXED_DELTA, VEGA_MATRIX, VOLATILITY, FLAT_DELTA};

	/**
	 * @return maps the lowercase columnName to Enum
	 */
	public static Metric toEnum (String columnName){
		return Metric.valueOf(columnName.toUpperCase());
	}
	
	@Override
	/**
	 * @return the lowercase representation DtccValue
	 */
	public String toString(){
		return super.toString().toLowerCase();
	}
}	
